function [t, x, v] = CALMProfile(x0, MaxSpeed, MaxTime)
%% [t, x, v] = CALMProfile(x0, MaxSpeed, MaxTime)
%% Returns positions and velocities as a function of time in [0, MaxTime] 
%% for a passenger who starts at a distance of x0 from another non-moving 
%% passenger with initial speed MaxSpeed.  

tspan = [0 MaxTime];
Y0 = [x0 MaxSpeed];

Threshold = 0.2;
DecelerationFactor = 0.4;
ReactionTime = 0.4; 
BreakingFactor = 460;

[t, y] = ode45(@(t, y) CALMDerivative(t, y,Threshold, DecelerationFactor, MaxSpeed, ReactionTime, BreakingFactor), tspan, Y0); 
 
x = y(:,1);
v = y(:, 2);