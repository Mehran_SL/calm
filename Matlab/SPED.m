avals = zeros(1,73);
bvals = zeros(1,73);
cvals = zeros(1,73);
x0vals = zeros(1,73);
ny = zeros(1,73);



[t, distance_1, velocity_1] = CALMProfile(4.0, 1.5, 45);

[t, distance_2, velocity_2] = CALMProfile(4.0, 1.0, 45);

[t, distance_3, velocity_3] = CALMProfile(4.0, 0.8, 45);




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                                                    %CALM%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

x=zeros(1, 750);
v_1=zeros(1, 25);
v_2=zeros(1, 25);
v_3=zeros(1, 25);

a = 2.11;
b = 0.366;
c = 0.966;


[x1, v_1] = CALMDynamics(1.5);
[x2, v_2] = CALMDynamics(1.0);
[x3, v_3] = CALMDynamics(0.8);
% 
% createfigure(x, v_1, v_2, v_3);
% showplottool('propertyeditor')



createSPEDfigure(distance_1, velocity_1, distance_2, velocity_2, distance_3, velocity_3, x1, v_1, x2, v_2, x3, v_3);
showplottool('propertyeditor');
 


