x=zeros(1, 30);
f_1=zeros(1, 30);
f_2=zeros(1, 30);
f_3=zeros(1, 30);

a = 2.11;
b = 0.366;
c = 0.966;


v=1.5;
x_0=4.0;
for i=1:52
    x(1, i)=x_0-i*0.07;
    beta = c-exp(-a*( x(1, i)-b));
    f_1(1, i) = ((beta-1)*v)/0.4;
end


v=1.0;
x_0=4.0;
for i=1:52
    x(1, i)=x_0-i*0.07;
    beta = c-exp(-a*( x(1, i)-b));
    f_2(1, i) = ((beta-1)*v)/0.4;
end


v=0.8;
x_0=4.0;
for i=1:52
    x(1, i)=x_0-i*0.07;
    beta = c-exp(-a*( x(1, i)-b));
    f_3(1, i) = ((beta-1)*v)/0.4;
end

createfigure(x, f_1, f_2, f_3);
showplottool('propertyeditor')