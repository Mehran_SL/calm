function [dydt] = CALMDerivative(t, Y, Threshold, DecelerationFactor, MaxSpeed, ReactionTime, BreakingFactor)
%% [dydt] = CALMDerivative(t, Y, Threshold, DecelerationFactor, MaxSpeed, ReactionTime, BreakingFactor)
%% Returns derivative of position and velocity for the CLAM model. t: Time,
%% Y: 2-D vector with current position and velocity, Threshold: theshold for breaking
%% DecelerationFactor: controls speed of deceleration, MaxSpeed: maximum speed at infinite
%% distance from nearest neighbor, ReactionTime: reaction time, 
%% BreakingFactor: controls rate of exponential breaking
%% Position is in metres from nearest neighbor and velocity is in m/s.
%% Typical values of parameters:
%% Threshold: 0.2m, DecelerationFactor: 0.4m, MaxSpeed: 1m/s, ReactionTime:0.4s, 
%% BreakingFactor: 460

dydt = zeros(2, 1);
dydt(1) = -Y(2);

if Y(1) < Threshold
    dydt(2) = -BreakingFactor*Y(2);
else
    dydt(2) = (MaxSpeed*(1.0 - DecelerationFactor/Y(1)) - Y(2))/ReactionTime;
end
