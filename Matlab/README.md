To generate the results of Fig 1, please run the CALM_Repulsion.m file.

To generate the results of Fig 3, please run the SPED.m file.

To generate the results of Fig 2, please run call this  code:

	[t, x, v] = CALM_Profile(4, 1.0, 45) 
Then use the curvefitting tool in Matlab to fit a curve with this the formulation of Equation (7) in the paper.