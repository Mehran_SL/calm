function [x, v_1] = CALMDynamics(v)

x=zeros(1, 500);
v_1=zeros(1, 500);


a = 2.11;
b = 0.366;
c = 0.966;


x(1, 1)=4.0;
v_1(1,1) = v;
v0=v;
for i=1:1500
    beta = c-exp(-a*( x(1, i)-b));
    f = (beta*v0-v)/0.4;
    v = (f * 0.005)+v;
    v_1(1, i+1) = v;
    x(1, i+1) = x(1,i)-v*0.005;
end