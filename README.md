**Constrained Linear Movement (CALM) model**

The CALM model can be used for simulating the pedestrian/crowd movements in narrow paths.

Please read the ReadMe files in the Boarding and Deplaning directories for more information about how to use the code for these two applications. 

You can also find the codes for generating the figures 1, 2 and 3 of the paper in the "Matlab" folder.

The implementation of the SPED model is also included in the SPED folder. PLese use the instructions in that folder for compiling the code for a parallel parameter sweep.